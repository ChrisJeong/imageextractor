# USAGE
# python find_shapes.py --image shapes.png

# import the necessary packages
import argparse
import re
import os
import shutil
import cv2
from image_extractor import ImageThread, ImageExtractorCallback

def main():
	imageNames = _image_names_from_args()

	threads = []
	for imageName in imageNames:
		dirName, extension = os.path.splitext(imageName)
		_create_directory(dirName)
		
		def save_image(cropped_image, src_image_name, index):
			fileName = "%02d%s" % ((index+1), extension)
			filePath = os.path.join(dirName, fileName)
			cv2.imwrite(filePath, cropped_image)
			
		def print_result(image_name, cropped_images):
			print "%s's cropped image count: %d" % (image_name, len(cropped_images))
		
		callback = ImageExtractorCallback(save_image, print_result)
		thread = ImageThread(image_name=imageName, callback = callback)
		thread.start()
		threads.append(thread)
		
	for thread in threads:
		thread.join()
		    
def _image_names_from_args():
	# construct the argument parse and parse the arguments
	ap = argparse.ArgumentParser()
	ap.add_argument("-i", "--image", help = "path to the image file", nargs='*')
	args = vars(ap.parse_args())

	regex = re.compile(r"\./")
	image_names = []
	for image_name in args["image"]:
		image_names.append(regex.sub("", image_name))

	# load the image
	return image_names

def _create_directory(dir_name):
	if os.path.exists(dir_name):
		shutil.rmtree(dir_name)

	os.makedirs(dir_name)

if __name__ == "__main__":
	main()
