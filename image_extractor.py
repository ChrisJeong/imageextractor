import numpy as np
import cv2
import time
import threading

class ImageThread(threading.Thread):
	def __init__(self, image_name, callback):
		self.image_name = image_name
		self.callback = callback
		threading.Thread.__init__(self)

	def run(self):
		print "%s working..." % self.getName()
		extract_and_save(self.image_name, self.callback)
		
class ImageExtractorCallback:
	def __init__(self, on_image_cropped, on_all_images_cropped, on_error_occurred = None, on_get_cropped_image_count = None):
		self.on_image_cropped = on_image_cropped
		self.on_all_images_cropped = on_all_images_cropped
		self.on_error_occurred = on_error_occurred
		self.on_get_cropped_image_count = on_get_cropped_image_count

def extract_and_save(image_name, callback):
	image = cv2.imread(image_name)

	try:
		contours = _extract_contours_from_image(image)
		candidate_rects = _cropped_image_candidate_rects(contours, callback)
	
		_save_cropped_images(image, image_name, candidate_rects, callback)
	except:
		callback.on_error_occurred()

def _extract_contours_from_image(image):
	# find all the 'black' shapes in the image
	lower = np.array([0, 0, 0])
	upper = np.array([240, 240, 240])
	shapeMask = cv2.inRange(image, lower, upper)

	(contours, _) = cv2.findContours(shapeMask.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)

	return contours

def _cropped_image_candidate_rects(contours, callback):
	candidates = []
	for c in contours:
		# draw the contour and show it
		rect = cv2.boundingRect(c)
		rectWidth = rect[2]
		rectHeight = rect[3]
		new_rect = (rect[0], rect[1], rect[2] + rect[0], rect[3] + rect[1])

		if rectWidth > 200 and rectHeight > 200:
			candidates.append(new_rect)

	candidates.reverse()
	
	if callback.on_get_cropped_image_count is not None:
		callback.on_get_cropped_image_count(len(candidates))

	return candidates

def _save_cropped_images(image, image_name, candidate_rects, callback):
	cropped_images = []
	for i in range(0, len(candidate_rects)):
		rect = candidate_rects[i]
		cropped_image = image[rect[1]:rect[3], rect[0]:rect[2]]

		time.sleep(0.1)
		callback.on_image_cropped(cropped_image, image_name, i)
		
		cropped_images.append(cropped_image)
	callback.on_all_images_cropped(image_name, cropped_images)
	
	return cropped_images
